from django.shortcuts import render, redirect
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import CreateTaskForm
from .models import Task


# Create your views here.
@login_required
def create_task(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request: HttpRequest) -> HttpResponse:
    my_task = Task.objects.filter(assignee=request.user)
    context = {"my_task": my_task}
    return render(request, "tasks/show_my_tasks.html", context)
