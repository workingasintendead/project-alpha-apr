from django.urls import path
from .views import list_projects, show_detail, create_project

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
