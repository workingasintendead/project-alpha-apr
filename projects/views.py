from django.shortcuts import render, redirect, get_object_or_404
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from .models import Project
from .forms import CreateProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_projects(request: HttpRequest) -> HttpResponse:
    project = Project.objects.filter(owner=request.user)
    context = {"project": project}
    return render(request, "projects/list.html", context)


@login_required
def show_detail(request: HttpRequest, id: int) -> HttpResponse:
    detail = get_object_or_404(Project, id=id)
    context = {"detail": detail}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
